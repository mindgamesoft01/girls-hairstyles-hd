package com.am.girls.app1.hairstyle.designs;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class CustomListAdapter1 extends BaseAdapter {
	int[] grid_data;
	Context context;

	public CustomListAdapter1(Context c, int[] gRID_DATA2) {
		context = c;
		grid_data = gRID_DATA2;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return grid_data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View gridView = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			gridView = new View(context);
			gridView = inflater.inflate(R.layout.grid_single, null);
		}

		// get layout from grid_item.xml ( Defined Below )

		ImageView imageView = (ImageView) gridView.findViewById(R.id.imageview);
		Bitmap drawableImage=BitmapFactory.decodeResource(context.getResources(),grid_data[position]);
		Bitmap bitmap = Bitmap.createScaledBitmap(drawableImage, 200, 200, false);
		Drawable drawableScaled = new BitmapDrawable(context.getResources(), bitmap);
		imageView.setImageDrawable(drawableScaled);

		return gridView;
	}
}
