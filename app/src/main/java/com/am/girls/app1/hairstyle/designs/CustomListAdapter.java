package com.am.girls.app1.hairstyle.designs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import static java.security.AccessController.getContext;

public class CustomListAdapter extends BaseAdapter {
	int[] grid_data;
	Context context;

	public CustomListAdapter(Context c, int[] gRID_DATA2) {
		context = c;
		grid_data = gRID_DATA2;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return grid_data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView = null;

		gridView = new View(context);


		// get layout from grid_item.xml ( Defined Below )

		gridView = inflater.inflate(R.layout.grid_single, null);
		ImageView imageView = (ImageView) gridView.findViewById(R.id.imageview);
//		Bitmap drawableImage = BitmapFactory.decodeResource(
//				context.getResources(), grid_data[position]);
//		Bitmap bitmap = Bitmap.createScaledBitmap(drawableImage, 200, 200,
//				false);
//		Drawable drawableScaled = new BitmapDrawable(context.getResources(),
//				bitmap);
//		imageView.setImageDrawable(drawableScaled);
		Glide.with(context).load(grid_data[position])
				.thumbnail(0.5f)
				.crossFade()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.into(imageView);


		return gridView;
	}
}
