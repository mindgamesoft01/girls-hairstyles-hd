package com.am.girls.app1.hairstyle.designs;

import java.io.File;
import java.io.FileOutputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.am.girls.app1.hairstyle.designs.multitouch.MultiTouchListener;

public class Settwo_Fullscreen extends Activity {
    Bitmap Sbitmap;
    ImageView imageview;
    int val;
    FileOutputStream fo;
    public Integer[] hand_designs = {R.drawable.img_46, R.drawable.img_47,
            R.drawable.img_48, R.drawable.img_49, R.drawable.img_50,
            R.drawable.img_51, R.drawable.img_52, R.drawable.img_53,
            R.drawable.img_54,
            R.drawable.img_58, R.drawable.img_59,
            R.drawable.img_61, R.drawable.img_62,
            R.drawable.img_63, R.drawable.img_65, R.drawable.img_66,
            R.drawable.img_67, R.drawable.img_68, R.drawable.img_69,
            R.drawable.img_70, R.drawable.img_71, R.drawable.img_72,
            R.drawable.img_73, R.drawable.img_74, R.drawable.img_75,
            R.drawable.img_76, R.drawable.img_77, R.drawable.img_78,
            R.drawable.img_79, R.drawable.img_80, R.drawable.img_81,
            R.drawable.img_82, R.drawable.img_83, R.drawable.img_84,
            R.drawable.img_85, R.drawable.img_86, R.drawable.img_87,
            R.drawable.img_89, R.drawable.img_90,};
    public int position = 0;
    public int totalimg = hand_designs.length;

    // these matrix will be used to zoom an image
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;

    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;
    Button share, rate, moreapp, whatsapp, save_image;

    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    private File newDir;
    private String fotoname;
    private File file;
    private String s;
    private FileOutputStream out;
    private String root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hand);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(this);
        TextView text = (TextView) findViewById(R.id.text);
        text.setText(R.string.settwo_styles);

		/*
         * layout1 = (LinearLayout) findViewById(R.id.admob1); strip1 =
		 * ad.layout_strip(this); layout1.addView(strip1);
		 * ad.AdMobBanner1(this);
		 */

        Intent i = getIntent();
        val = i.getIntExtra("ganeshcard", 0);
        Sbitmap = BitmapFactory.decodeResource(getResources(),
                hand_designs[position]);

        Bundle b = getIntent().getExtras();
        position = b.getInt("flag");

        imageview = (ImageView) findViewById(R.id.imageview1);

        imageview.setBackgroundResource(hand_designs[position]);
        imageview.setOnTouchListener(new MultiTouchListener());

        imageview.setOnTouchListener(new MultiTouchListener());

    }

    public void next1(View v) {
        val = 0;
        try {
            if (position < totalimg) {

                imageview.setBackgroundResource(hand_designs[position++]);
                if (position == totalimg - 2) {

                    // ad.AdMobInterstitial(FootActivity.this);
                }

            } else {
                Toast.makeText(getApplicationContext(),
                        "There is no Image to show", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void pre1(View v) {
        val = 0;
        try {
            if (position > 0) {

                imageview.setBackgroundResource(hand_designs[position--]);

            } else {
                Toast.makeText(getApplicationContext(),
                        "There is no Image to show", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        Intent i = new Intent(Settwo_Fullscreen.this, Settwo.class);
        startActivity(i);
        return super.onKeyDown(keyCode, event);
    }

}
