package  com.am.girls.app1.hairstyle.designs;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SecondActivity extends Activity {

	LinearLayout layout, strip, layout1, strip1;
	AdClass ad = new AdClass();
	TextView terms_privacy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);

		layout = (LinearLayout) findViewById(R.id.admob);
		strip = ad.layout_strip(this);
		layout.addView(strip);
		ad.AdMobBanner(this);


		layout1 = (LinearLayout) findViewById(R.id.admob1);
		strip1 = ad.layout_strip(this);
		layout1.addView(strip1);
		ad.AdMobBanner(this);

		terms_privacy=(TextView)findViewById(R.id.privacy);
		terms_privacy.setPaintFlags(terms_privacy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

		terms_privacy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(SecondActivity.this, PrivacyPolicy.class);
				startActivity(i);
			}
		});

	}

	public void gallery(View v) {
		Intent i = new Intent(SecondActivity.this, GalleryActivity.class);

		startActivity(i);

	}

	

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}

		return super.onKeyDown(keyCode, event);
	}

}
