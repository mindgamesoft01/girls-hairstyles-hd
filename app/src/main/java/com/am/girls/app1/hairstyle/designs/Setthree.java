package com.am.girls.app1.hairstyle.designs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Setthree extends Activity {

    public int[] GRID_DATA_set_two = {R.drawable.img_91, R.drawable.img_92,
            R.drawable.img_93, R.drawable.img_94, R.drawable.img_95,
            R.drawable.img_96, R.drawable.img_97, R.drawable.img_98,
            R.drawable.img_100, R.drawable.img_101, R.drawable.img_102,
            R.drawable.img_104, R.drawable.img_105,
            R.drawable.img_106, R.drawable.img_107, R.drawable.img_108,
            R.drawable.img_109, R.drawable.img_110, R.drawable.img_111,
            R.drawable.img_112, R.drawable.img_113,};
    GridView grid;
    int flag;

    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        grid = (GridView) findViewById(R.id.gridview);

        TextView text = (TextView) findViewById(R.id.text);
        text.setText(R.string.setthree_styles);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(this);

        grid.setAdapter(new CustomListAdapter(this, GRID_DATA_set_two));
        grid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
               // ad.AdMobInterstitial1(Setthree.this);
                Intent i = new Intent(Setthree.this, Setthree_Fullscreen.class);
                Bundle b = new Bundle();
                b.putInt("flag", position);
                i.putExtras(b);
                startActivity(i);

            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        Intent i = new Intent(Setthree.this, GalleryActivity.class);
        startActivity(i);
        return super.onKeyDown(keyCode, event);
    }

}
