package  com.am.girls.app1.hairstyle.designs;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;

import static com.google.android.gms.wearable.DataMap.TAG;

public class GalleryActivity extends Activity {
	Button btn1,btn2,btn3,btn4;

	LinearLayout layout, strip, layout1, strip1;
	AdClass ad = new AdClass();
	ProgressDialog pd;
	long current_time;
	InterstitialAd interstitialAd;
	String AD_UNIT_ID_full_page = "ca-app-pub-4884901225577284/5598315856";

	InterstitialAd interstitialAd1;
	String AD_UNIT_ID_full_page1 = "ca-app-pub-4884901225577284/8551782259";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);

		final ProgressDialog pd = new ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setMessage("Please wait ..."+ "Ad Loading ...");
		pd.setIndeterminate(true);
		interstitialAd=new InterstitialAd(this);
		interstitialAd1=new InterstitialAd(this);

		final AdRequest adRequest = new AdRequest.Builder().build();
		interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

		final AdRequest adRequest1 = new AdRequest.Builder().build();
		interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);

		btn1=(Button)findViewById(R.id.btn1);
		btn2=(Button)findViewById(R.id.btn2);
		btn3=(Button)findViewById(R.id.btn3);

		layout = (LinearLayout) findViewById(R.id.admob);
		strip = ad.layout_strip(this);
		layout.addView(strip);
		ad.AdMobBanner(this);


		current_time=new Date().getTime();

		btn1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				pd.show();
				//interstitialAd.setAdUnitId(interstitial0);
				Boolean ad1_status = singleton_images.getInstance().get_ad_status(0);
				Log.e(TAG,ad1_status.toString());
				if(ad1_status==Boolean.FALSE){
					Intent i = new Intent(GalleryActivity.this, Setone.class);
					startActivity(i);
					pd.dismiss();


				}



				if(ad1_status==Boolean.TRUE) {
					interstitialAd.setAdListener(new AdListener() {
						@Override
						public void onAdClosed() {
							super.onAdClosed();


							Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();
							Intent i = new Intent(GalleryActivity.this, Setone.class);
							startActivity(i);
							pd.dismiss();


						}

						@Override
						public void onAdFailedToLoad(int i) {
							super.onAdFailedToLoad(i);

							//set the last loaded timestamp even if the ad load fails
							singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

						}

						@Override
						public void onAdLeftApplication() {
							super.onAdLeftApplication();
						}

						@Override
						public void onAdOpened() {
							super.onAdOpened();
						}

						@Override
						public void onAdLoaded() {
							super.onAdLoaded();
							//set the last loaded timestamp
							singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

							interstitialAd.show();
							pd.dismiss();

						}
					});
					interstitialAd.loadAd(adRequest);
				}

			}
		});

		btn2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				pd.show();
				//pd.setCancelable(true);
				//interstitialAd.setAdUnitId(interstitial1);
				Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
				Log.e(TAG,ad1_status.toString());
				if(ad1_status==Boolean.FALSE){
					Intent i = new Intent(GalleryActivity.this, Settwo.class);
					startActivity(i);
					pd.dismiss();


				}
				if(ad1_status==Boolean.TRUE) {
					interstitialAd.setAdListener(new AdListener() {
						@Override
						public void onAdClosed() {
							super.onAdClosed();

							Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

							Intent i = new Intent(GalleryActivity.this, Settwo.class);
							startActivity(i);
							pd.dismiss();
						}

						@Override
						public void onAdFailedToLoad(int i) {
							super.onAdFailedToLoad(i);
							//set the last loaded timestamp even if the ad load fails
							singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

						}

						@Override
						public void onAdLeftApplication() {
							super.onAdLeftApplication();
						}

						@Override
						public void onAdOpened() {
							super.onAdOpened();
						}

						@Override
						public void onAdLoaded() {
							super.onAdLoaded();
							//set the last loaded timestamp even if the ad load fails
							singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

							interstitialAd.show();
							pd.dismiss();

						}
					});
					interstitialAd.loadAd(adRequest);
				}
			}
		});
		btn3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				pd.show();
				//pd.setCancelable(true);
				//interstitialAd.setAdUnitId(interstitial2);
				Boolean ad1_status = singleton_images.getInstance().get_ad_status(2);
				Log.e(TAG,ad1_status.toString());
				if(ad1_status==Boolean.FALSE){
					Intent i = new Intent(GalleryActivity.this, Setthree.class);
					startActivity(i);
					pd.dismiss();


				}


				if(ad1_status==Boolean.TRUE) {

					interstitialAd1.setAdListener(new AdListener() {
						@Override
						public void onAdClosed() {
							super.onAdClosed();


							Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

							Intent i = new Intent(GalleryActivity.this, Setthree.class);
							startActivity(i);
							pd.dismiss();
						}

						@Override
						public void onAdFailedToLoad(int i) {

							super.onAdFailedToLoad(i);
							//set the last loaded timestamp even if the ad load fails
							singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

						}

						@Override
						public void onAdLeftApplication() {
							super.onAdLeftApplication();
						}

						@Override
						public void onAdOpened() {
							super.onAdOpened();
						}

						@Override
						public void onAdLoaded() {
							super.onAdLoaded();
							Toast.makeText(getApplicationContext(), "Thanks for watching add...", Toast.LENGTH_LONG).show();

							//set the last loaded timestamp even if the ad load fails
							singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

							interstitialAd1.show();
							pd.dismiss();

						}
					});
					interstitialAd1.loadAd(adRequest1);
				}
			}
		});

		

	}

//	public void setone(View v) {
//		pd.show();
//		ad.interstitial.setAdListener(new AdListener() {
//			@Override
//			public void onAdClosed() {
//				super.onAdClosed();
//				Toast.makeText(getApplicationContext(),"Thanks for watching add...",Toast.LENGTH_LONG).show();
//				Intent i = new Intent(GalleryActivity.this, Setone.class);
//				startActivity(i);
//				pd.dismiss();
//			}
//
//			@Override
//			public void onAdFailedToLoad(int i) {
//				super.onAdFailedToLoad(i);
//				singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//			}
//
//			@Override
//			public void onAdLeftApplication() {
//				super.onAdLeftApplication();
//			}
//
//			@Override
//			public void onAdOpened() {
//				super.onAdOpened();
//			}
//
//			@Override
//			public void onAdLoaded() {
//				super.onAdLoaded();
//				singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//				ad.interstitial.show();
//				pd.dismiss();
//			}
//
//		});
//
//
//
//	}
//
//	public void settwo(View v) {
//		 //ad.AdMobInterstitial(GalleryActivity.this);
//
//		Intent i = new Intent(GalleryActivity.this, Settwo.class);
//
//		startActivity(i);
//	}
//	public void setthree(View v) {
//		 ad.AdMobInterstitial(GalleryActivity.this);
//
//		Intent i = new Intent(GalleryActivity.this, Setthree.class);
//
//		startActivity(i);
//	}

	

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			/*
			 * Intent intent = new Intent(Intent.ACTION_MAIN);
			 * intent.addCategory(Intent.CATEGORY_HOME);
			 * intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 * startActivity(intent);
			 */

			Intent i = new Intent(GalleryActivity.this, SecondActivity.class);
			startActivity(i);
		}

		return super.onKeyDown(keyCode, event);
	}

}
