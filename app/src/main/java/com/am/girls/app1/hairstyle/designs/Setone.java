package  com.am.girls.app1.hairstyle.designs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Setone extends Activity {

	public int[] GRID_DATA_set_one = { R.drawable.img_1, R.drawable.img_2,
			R.drawable.img_3, R.drawable.img_5, R.drawable.img_6,
			R.drawable.img_7, R.drawable.img_8, R.drawable.img_9,
			R.drawable.img_10, R.drawable.img_11, R.drawable.img_12,
			R.drawable.img_13, R.drawable.img_14, R.drawable.img_15,
			R.drawable.img_16, R.drawable.img_17, R.drawable.img_18,
			R.drawable.img_19, R.drawable.img_20, R.drawable.img_21,
			R.drawable.img_22, R.drawable.img_23, R.drawable.img_24,
			R.drawable.img_25, R.drawable.img_26, R.drawable.img_28,
			R.drawable.img_29, R.drawable.img_30, R.drawable.img_31,
			R.drawable.img_32, R.drawable.img_33, R.drawable.img_34,
			R.drawable.img_35, R.drawable.img_36, R.drawable.img_38,
			R.drawable.img_39, R.drawable.img_40, R.drawable.img_41,
			R.drawable.img_42, R.drawable.img_43, R.drawable.img_44,
			R.drawable.img_45, };
	LinearLayout layout, strip, layout1, strip1;
	AdClass ad = new AdClass();

	GridView grid;
	int flag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grid);
		grid = (GridView) findViewById(R.id.gridview);
		TextView text = (TextView) findViewById(R.id.text);
		text.setText(R.string.setone_styles);

		layout = (LinearLayout) findViewById(R.id.admob);
		strip = ad.layout_strip(this);
		layout.addView(strip);
		ad.AdMobBanner(this);

		grid.setAdapter(new CustomListAdapter(this, GRID_DATA_set_one));
		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				//ad.AdMobInterstitial(Setone.this);
				Intent i = new Intent(Setone.this, Setone_fullscreen.class);
				Bundle b = new Bundle();
				b.putInt("flag", position);
				i.putExtras(b);
				startActivity(i);

			}
		});

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			/*
			 * Intent intent = new Intent(Intent.ACTION_MAIN);
			 * intent.addCategory(Intent.CATEGORY_HOME);
			 * intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 * startActivity(intent);
			 */

			Intent i = new Intent(Setone.this, GalleryActivity.class);
			startActivity(i);
		}

		return super.onKeyDown(keyCode, event);
	}
}
