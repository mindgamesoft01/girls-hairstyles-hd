package com.am.girls.app1.hairstyle.designs;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kalyani on 5/4/2017.
 */

class singleton_images {
    public long lastIntestitial_loadded=0;

    public String getApp_name() {
        return app_name;
    }

    public int getInterstitial_timer() {
        return interstitial_timer;
    }

    public void setInterstitial_timer(int interstitial_timer) {
        this.interstitial_timer = interstitial_timer;
    }

    public int interstitial_timer=50;

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String app_name = "";

    public long getLastIntestitial_loadded() {
        return lastIntestitial_loadded;
    }

    public void setLastIntestitial_loadded(long lastIntestitial_loadded) {
        this.lastIntestitial_loadded = lastIntestitial_loadded;
    }






    public Log getIntertitial_lastloaded() {
        return intertitial_lastloaded;
    }

    public void setIntertitial_lastloaded(Log intertitial_lastloaded) {
        this.intertitial_lastloaded = intertitial_lastloaded;
    }

    Log intertitial_lastloaded;
    private static final singleton_images ourInstance = new singleton_images();

    public JSONArray getImages_json() {
        return images_json;
    }

    public void setImages_json(JSONArray images_json) {
        Log.d("images", images_json.toString());
        this.images_json = images_json;
    }

    public JSONArray images_json;

    public JSONObject getAdmob_details() {
        return admob_details;
    }

    public void setAdmob_details(JSONObject admob_details) {
        this.admob_details = admob_details;
    }

    public JSONObject admob_details;

    public JSONArray getAd_details() {
        return ad_token;
    }

    public void setAd_details(JSONArray ad_token) {
        this.ad_token = ad_token;
    }

    private JSONArray ad_token;

    public void setAd_details(JSONObject ad_details) {
        this.ad_details = ad_details;
    }

    private JSONObject ad_details;

    static singleton_images getInstance() {
        return ourInstance;
    }

    private singleton_images() {
   }
   /*Returns an array list with set images*/
   public ArrayList<String> get_sets(){
       ArrayList<String> set_images = new ArrayList<String>();
       try {

           for (int i=0; i<images_json.length() ;i++){

               String set_url = images_json.getJSONObject(i).getString("bucket") + images_json.getJSONObject(i).getString("set_image");
               set_images.add( set_url);
           }


       }
       catch (Exception e){
           e.printStackTrace();
       }

       return set_images;
   }

    /*Returns an array list with child images for a set*/
    public ArrayList<String> get_set_images(int index){
        ArrayList<String> set_images = new ArrayList<String>();
        try {
            Log.d("get_set_imaeges", images_json.toString());
                JSONArray tmp =  images_json.getJSONObject(index).getJSONArray("child_images");
                String bucket = images_json.getJSONObject(index).getString("bucket"); /*Bucket is same for all*/
                String folder = images_json.getJSONObject(index).getString("folder"); /*Folder is same for all*/
            for(int i =0; i<tmp.length(); i++ ){
                String set_img_url =bucket + folder+"/" + tmp.getString(i);
                set_images.add(set_img_url);
            }



        }
        catch (Exception e){
            e.printStackTrace();
        }

        return set_images;
    }


    public Boolean get_ad_status(int index) {
        Boolean ad_status = Boolean.TRUE;
        String ture="true";
        String fasle="false";
        try {


            /*Check the time stamp difference*/
            if (lastIntestitial_loadded != 0) {
                //Diff in seconds
                long difference = (new Date().getTime() - lastIntestitial_loadded) / 1000;

                if ((difference >= interstitial_timer)) {
                    ad_status = Boolean.TRUE;
                } else {
                    ad_status = Boolean.FALSE;
                }
            } else {
                ad_status = Boolean.TRUE;
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
        return ad_status;
    }

    /*---------------------------------------------------------------------------*
     *
      *@param index - index of the menu item
     * @param adunit - Values - interstitial/banner
     * @return
     * sample => get_admob_id(0, "interstitial"),get_admob_id(0, "banner")
     **---------------------------------------------------------------------------*/
    public String get_admob_id(int index, String adunit) {
        String adunit_id = "";
        try {
            JSONArray tmp = admob_details.getJSONArray(adunit);
            adunit_id = tmp.getJSONArray(index).toString();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("get_admob_id","Unable to get the admob ad id.");
        }

        return adunit_id;
    }
}
