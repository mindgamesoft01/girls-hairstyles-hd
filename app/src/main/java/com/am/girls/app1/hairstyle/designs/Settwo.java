package com.am.girls.app1.hairstyle.designs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Settwo extends Activity {

    public int[] GRID_DATA_set_two = {R.drawable.img_46, R.drawable.img_47,
            R.drawable.img_48, R.drawable.img_49, R.drawable.img_50,
            R.drawable.img_51, R.drawable.img_52, R.drawable.img_53,
            R.drawable.img_54,
            R.drawable.img_58, R.drawable.img_59,
            R.drawable.img_61, R.drawable.img_62,
            R.drawable.img_63, R.drawable.img_65, R.drawable.img_66,
            R.drawable.img_67, R.drawable.img_68, R.drawable.img_69,
            R.drawable.img_70, R.drawable.img_71, R.drawable.img_72,
            R.drawable.img_73, R.drawable.img_74, R.drawable.img_75,
            R.drawable.img_76, R.drawable.img_77, R.drawable.img_78,
            R.drawable.img_79, R.drawable.img_80, R.drawable.img_81,
            R.drawable.img_82, R.drawable.img_83, R.drawable.img_84,
            R.drawable.img_85, R.drawable.img_86, R.drawable.img_87,
            R.drawable.img_89, R.drawable.img_90,};
    GridView grid;
    int flag;

    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        grid = (GridView) findViewById(R.id.gridview);

        TextView text = (TextView) findViewById(R.id.text);
        text.setText(R.string.settwo_styles);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(this);

        grid.setAdapter(new CustomListAdapter1(this, GRID_DATA_set_two));
        grid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //ad.AdMobInterstitial1(Settwo.this);
                Intent i = new Intent(Settwo.this, Settwo_Fullscreen.class);
                Bundle b = new Bundle();
                b.putInt("flag", position);
                i.putExtras(b);
                startActivity(i);

            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        Intent i = new Intent(Settwo.this, GalleryActivity.class);
        startActivity(i);
        return super.onKeyDown(keyCode, event);
    }

}
