package  com.am.girls.app1.hairstyle.designs;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class AdClass {

	Context c;
	LinearLayout layout, layout1;
	// AdMOb
	AdView adView;
	String AD_UNIT_ID = "ca-app-pub-4884901225577284/2644849450";

	AdView adView1;
	String AD_UNIT_ID1 = "ca-app-pub-4884901225577284/4121582654";

	InterstitialAd interstitial;
	String AD_UNIT_ID_full_page = "ca-app-pub-4884901225577284/5598315856";

	InterstitialAd interstitial1;
	String AD_UNIT_ID_full_page1 = "ca-app-pub-4884901225577284/8551782259";
	
	

	public LinearLayout layout_strip(Context context) {

		c = context;
		Log.d("TASG", "entered layout code");

		layout = new LinearLayout(c);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		layout.setLayoutParams(params);
		layout.setOrientation(LinearLayout.HORIZONTAL);

		return layout;
	}

	public void AdMobBanner(Context c) {

		adView = new AdView(c);
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId(AD_UNIT_ID);

		layout.addView(adView);

		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

	}

	public void AdMobBanner1(Context c) {

		adView1 = new AdView(c);
		adView1.setAdSize(AdSize.SMART_BANNER);
		adView1.setAdUnitId(AD_UNIT_ID1);

		layout.addView(adView1);

		AdRequest adRequest1 = new AdRequest.Builder().build();
		adView1.loadAd(adRequest1);

	}

	public void AdMobInterstitial(Context c) {
		interstitial = new InterstitialAd(c);
		interstitial.setAdUnitId(AD_UNIT_ID_full_page);

		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();
		// Begin loading your interstitial.
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				interstitial.show();
			}
		});

	}

	public void AdMobInterstitial1(Context c) {
		interstitial1 = new InterstitialAd(c);
		interstitial1.setAdUnitId(AD_UNIT_ID_full_page1);

		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();
		// Begin loading your interstitial.
		interstitial1.loadAd(adRequest);
		interstitial1.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				interstitial1.show();
			}
		});

	}

	/*
	 * // RevMob // RevMob
	 * 
	 * public void RevMobBanner(Activity activity) {
	 * 
	 * revmob = RevMob.start(activity); banner = revmob.createBanner(activity);
	 * // layout.addView(banner); revmob.showBanner(activity);
	 * 
	 * }
	 * 
	 * public void RevMobFullPageAd(Activity activity) {
	 * 
	 * revmob = RevMob.start(activity); revmob.showFullscreen(activity);
	 * 
	 * }
	 * 
	 * 
	 * public void StartAppAds(Activity activity) { startAppAd = new
	 * StartAppAd(activity); StartAppSDK.init(activity, "110437331",
	 * "203250348", true); startAppAd.showAd(); startAppAd.loadAd();
	 * startAppAd.onBackPressed(); startAppAd.onResume(); startAppAd.onPause();
	 * }
	 * 
	 * public void StartAppFullPage(Activity activity) { startAppAd = new
	 * StartAppAd(activity); StartAppSDK.init(activity, "110437331",
	 * "203250348", true); startAppAd.showAd(); startAppAd.loadAd(); }
	 * 
	 * public void StartAppOnBack(Activity activity) {
	 * 
	 * startAppAd.onBackPressed(); }
	 * 
	 * // MM Ads
	 * 
	 * public void MMAds(Activity activity) {
	 * 
	 * mmb = new MMBestApps(activity); mmb.ShowDynamicAppWall(activity); mm =
	 * new MM(activity);
	 * 
	 * }
	 * 
	 * public void MMAppWall(Activity activity) {
	 * 
	 * mm = new MM(activity); mm.ShowAppWall(activity);
	 * 
	 * }
	 */

}
