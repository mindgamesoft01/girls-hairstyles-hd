package  com.am.girls.app1.hairstyle.designs;

import java.io.File;
import java.io.FileOutputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.am.girls.app1.hairstyle.designs.multitouch.MultiTouchListener;

@SuppressWarnings("deprecation")
public class Setone_fullscreen extends Activity {
	Bitmap Sbitmap;
	FileOutputStream fo;
	ImageView imageview;
	public static Bitmap localBitmap;
	Button share, rate, moreapp, whatsapp, next_button, next_add, save_imag;
	public Integer[] hand_designs = { R.drawable.img_1, R.drawable.img_2,
			R.drawable.img_3, R.drawable.img_5, R.drawable.img_6,
			R.drawable.img_7, R.drawable.img_8, R.drawable.img_9,
			R.drawable.img_10, R.drawable.img_11, R.drawable.img_12,
			R.drawable.img_13, R.drawable.img_14, R.drawable.img_15,
			R.drawable.img_16, R.drawable.img_17, R.drawable.img_18,
			R.drawable.img_19, R.drawable.img_20, R.drawable.img_21,
			R.drawable.img_22, R.drawable.img_23, R.drawable.img_24,
			R.drawable.img_25, R.drawable.img_26, R.drawable.img_28,
			R.drawable.img_29, R.drawable.img_30, R.drawable.img_31,
			R.drawable.img_32, R.drawable.img_33, R.drawable.img_34,
			R.drawable.img_35, R.drawable.img_36, R.drawable.img_38,
			R.drawable.img_39, R.drawable.img_40, R.drawable.img_41,
			R.drawable.img_42, R.drawable.img_43, R.drawable.img_44,
			R.drawable.img_45,  };
	public int position;
	public int totalimg = hand_designs.length;

	// these matrix will be used to zoom an image
	private Matrix matrix = new Matrix();
	private Matrix savedMatrix = new Matrix();
	// we can be in one of these 3 states
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;

	private int mode = NONE;
	// remember some things for zooming
	private PointF start = new PointF();
	private PointF mid = new PointF();
	private float oldDist = 1f;
	private float d = 0f;
	private float newRot = 0f;
	private float[] lastEvent = null;

	LinearLayout layout, strip, layout1, strip1;
	AdClass ad = new AdClass();
	private File newDir;
	private String fotoname;
	private File file;
	private String s;
	private FileOutputStream out;
	private int val;
	private String root;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hand);
		imageview = (ImageView) findViewById(R.id.imageview1);
		share = (Button) findViewById(R.id.share);
		rate = (Button) findViewById(R.id.rate);

		Bundle b = getIntent().getExtras();
		position = b.getInt("flag");

		Sbitmap = BitmapFactory.decodeResource(getResources(),
				hand_designs[position]);
		imageview.setBackgroundResource(hand_designs[position]);

		layout = (LinearLayout) findViewById(R.id.admob);
		strip = ad.layout_strip(this);
		layout.addView(strip);
		ad.AdMobBanner(this);
		TextView text = (TextView) findViewById(R.id.text);
		text.setText(R.string.setone_styles);

		/*
		 * layout1 = (LinearLayout) findViewById(R.id.admob1); strip1 =
		 * ad.layout_strip(this); layout1.addView(strip1);
		 * ad.AdMobBanner1(this);
		 */

		imageview.setOnTouchListener(new MultiTouchListener());

	}

	public void next1(View v) {
		try {
			if (position < totalimg) {
				val = 0;

				imageview.setBackgroundResource(hand_designs[position++]);

			} else {
				Toast.makeText(getApplicationContext(),
						"There is no Image to show", Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void pre1(View v) {
		try {
			if (position > 0) {
				val = 0;
				imageview.setBackgroundResource(hand_designs[position--]);

			} else {
				Toast.makeText(getApplicationContext(),
						"There is no Image to show", Toast.LENGTH_SHORT).show();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private float rotation(MotionEvent event) {
		double delta_x = (event.getX(0) - event.getX(1));
		double delta_y = (event.getY(0) - event.getY(1));
		double radians = Math.atan2(delta_y, delta_x);

		return (float) Math.toDegrees(radians);
	}

	@SuppressLint("FloatMath")
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(x * x + y * y);

	}

	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		Intent i = new Intent(Setone_fullscreen.this, Setone.class);
		startActivity(i);
		return super.onKeyDown(keyCode, event);
	}

}
